<?php

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;

trait ApiResponser{

    protected function successResponse($data =null,$list=null, $message = null, $code = 200)
	{
		return response()->json([
			'status'=> 'Success', 
			'message' => $message, 
			'model' => $data,
			'list' => $list,
			'returnedCode' => $code
		]);
	}

	protected function errorResponse($message = null, $code,$data=null,$list=null)
	{
		return response()->json([
			'status'=>'Error',
			'message' => $message,
			'model' => $data,
			'list' => $list,
			'returnedCode' => $code
		] );
	}

}
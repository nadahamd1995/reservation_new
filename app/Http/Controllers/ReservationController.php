<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Reservation;
use App\Models\EmpService;

class ReservationController extends ApiController
{
      //
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

     /**
     * Create a new ReservationController instance.
     *
     * @return void
     */
    
    public function index(Request $request)
 {
        $data = Reservation::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$data, $msg, 200 );
    }

    public function getById( Request $request )
 {
        $reservation = Reservation::where( 'id', $request->id )->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( $reservation,null,$msg, 200 );
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function create()
 {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function store( Request $request )
 {
        $reservation = Reservation::create( [
            'hour'=>$request->hour,
            'date' =>$request->date,
            'center_id'=>$request->center_id,
            'user_id'=>$request->user_id,
            'service_id'=>$request->service_id
        ] );
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse( $reservation,null, $msg, 200 );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Reservation  $reservation
    * @return \Illuminate\Http\Response
    */

    public function show( Reservation $reservation )
 {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Reservation  $reservation
    * @return \Illuminate\Http\Response
    */

    public function edit( Reservation $reservation )
 {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Reservation  $reservation
    * @return \Illuminate\Http\Response
    */

    public function update( Request $request, Reservation $reservation )
 {
        $request->validate( [           
            'hour'=>'required',
            'date' =>'required',
            'center_id'=>'required',
            'user_id'=>'required',
            'service_id'=>'required'
             ] );
        $reservation = Reservation::find( $request->id );
        $reservation->hour = $request->hour;
        $reservation->date = $request->date;
        $reservation->center_id = $request->center_id;
        $reservation->user_id = $request->user_id;
        $reservation->service_id = $request->service_id;
        $reservation->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse( $reservation,null, $msg, 200 );
    }

    public function deleteById( $id )
 {
        // $id = ;
        $reservation = Reservation::find( $id );
        // $advertisement = Advertisement::find( $request->id );
        if ( is_null( $reservation ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
            return  $this->errorResponse( $msg, null );
        } else {
            $res = $reservation->delete();
            if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                return $this->successResponse( $reservation,null, $msg, 200 );
            } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return  $this->errorResponse( $msg, null );
            }
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Reservation  $reservation
    * @return \Illuminate\Http\Response
    */

    public function destroy( Reservation $reservation )
 {
        $reservation->delete();
        $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
        return $this->successResponse( $reservation,null, $msg, 200 );
    }

    public function getreservationByCenterId(Request $request){
        $data=[];
        if($request->employee_id != null )
        {
            $notAvailable=Reservation::join('emp_services','emp_services.id','=','reservations.emp_services_id')
            ->where('reservations.center_id','=',$request->center_id)
            ->where('emp_services.employee_id',$request->employee_id)
            ->where('reservations.hour','=',$request->hour)
            ->where('reservations.date','=',$request->date)->get();
        $available=EmpService::join('services','emp_services.service_id','=','services.id')
            ->whereNotIn('emp_services.service_id',$notAvailable)
            ->where('services.center_id','=',$request->center_id)
            ->where('services.propertyCenter_id','=',$request->propertyCenter_id)->get();
        $data['available']=$available;
        $data['notAvailable']=$notAvailable;
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( $data ,null, $msg, 200 );
    }
     

    else{
        $notAvailable=Reservation::join('emp_services','emp_services.id','=','reservations.emp_services_id')
                                    ->where('reservations.center_id','=',$request->center_id)
                                    ->whereNull('emp_services.employee_id')
                                    ->where('reservations.hour','=',$request->hour)
                                    ->where('reservations.date','=',$request->date)->get();
        $available=EmpService::join('services','emp_services.service_id','=','services.id')
                                  ->whereNotIn('emp_services.service_id',$notAvailable)
                                  ->where('services.center_id','=',$request->center_id)
                                  ->where('services.propertyCenter_id','=',$request->propertyCenter_id)->get();
        $data['available']=$available;
        $data['notAvailable']=$notAvailable;
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( $data ,null, $msg, 200 );
    }
      
        
    }
}

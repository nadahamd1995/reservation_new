<?php

namespace App\Http\Controllers;
use Lang;
use App\Http\Controllers\FileController;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;


class PostController extends ApiController
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index(Request $request)
   {

    /**get post with children */
       $data = Post::with('center')->get();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse( null,$data, $msg, 200 );
   }


   public function getById( Request $request )
   {
          $post = Post::where( 'id', $request->id )->with('center')->get();
          $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
          return $this->successResponse( $post, null,$msg, 200 );
      }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
      $file=$request->file('file');
      $uploaded =FileController::uploadFile($file);
       $post = Post::create( [
           'content' => $request->content,
           'image' => $uploaded->getData()->file_url,
           'date'=> $request->date,
           'tag'=> $request->tag,
           'center_id' =>$request->center_id,
           'creator_id'=>Auth::id(),
       ] );
       $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
       return $this->successResponse( $post,null, $msg, 200 );
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Models\Post  $post
    * @return \Illuminate\Http\Response
    */
   public function show(Post $post)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Post  $post
    * @return \Illuminate\Http\Response
    */
   public function edit(Post $post)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Post  $post
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Post $post)
   {
       $request->validate( [
           'content' => 'required',
           'tag'=> 'required',
           'center_id'=>'required'
       ] );
       $creator_post_id=Auth::id();
       error_log('--------------------');
       error_log($creator_post_id);
       $post = Post::find( $request->id );
       if($post->creator_id==$creator_post_id)
       {
        $file=$request->file('file');
        $imgURL=$post->image;
        $fileName=FileController::findFileName($imgURL);
        $deleteFile=FileController::deleteFile($fileName);
        $uploadNew=FileController::uploadFile($file);
        $post->content = $request->content;
        $post->tag = $request->tag;
        $post->image=$uploadNew->getData()->file_url;
        $post->center_id = $request->center_id;
        $post->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse( $post,null, $msg, 200 );}

        else{
            $msg=\Lang::get('messages.NoPermission',[],$request->lang);
            return $this->errorResponse($msg,403);
        }
      
   }


   public function deleteById( Request $request )
   {
          $post = Post::find( $id );
          if ( is_null( $post ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
              return  $this->errorResponse( $msg, null );
          } else {
              $res = $post->delete();
              if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                  return $this->successResponse( $post,null, $msg, 200 );
              } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                  return  $this->errorResponse( $msg, null );
              }
          }
      }


      public function getByCenterCategory(Request $request )
      {
          $data = Post::join('centers', 'centers.id', '=', 'posts.center_id')
          ->join('categories', 'categories.id', '=', 'centers.category_id')
          ->where('category_id','=', $request->category_id)->orderBy('posts.created_at', 'DESC')->with('center')->get();
          $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
          return $this->successResponse( null,$data, $msg, 200 );
      }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Post  $post
    * @return \Illuminate\Http\Response
    */
   public function destroy(Post $post)
   {
       $post->delete();
   }

   public function searchBytag(Request $request){
    $data=Post::where('tag','LIKE','%'.$request->tag.'%')->with('center')->get();
    $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
    return $this->successResponse( null,$data, $msg, 200 );
   }

   public function searchByCenterName(Request $request){
    $data=Post::join('centers', 'centers.id', '=', 'posts.center_id')
                ->where('centers.name','LIKE','%'.$request->centerName.'%')->with('center')->get();
                $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
    return $this->successResponse( null,$data, $msg, 200 );
   }
}

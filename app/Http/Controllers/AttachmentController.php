<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FileController;
use App\Http\Requests\UpdateAttachmentRequest;
use App\Models\Attachment;
use Illuminate\Http\Request;

class AttachmentController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreAttachmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

    }


    public function addList($request,$center)
    {
        //

        if (!$request->hasFile('attachments')) {
            return response()->json(['upload_file_not_found'], 400);
        }

        $attachments = $request->file('attachments');
        error_log(count( $attachments));
        foreach ($attachments as $attachment) {
            $uploaded = FileController::uploadFile($attachment);
            $attachment = Attachment::create([
                'center_id' => $center->id,
                'link' => $uploaded->getData()->file_url,
            ]);
        }
        // $msg = \Lang::get('messages.AddedSuccessfully');
        // return $this->successResponse(null, null, $msg, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function show(Attachment $attachment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function edit(Attachment $attachment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAttachmentRequest  $request
     * @param  \App\Models\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAttachmentRequest $request, Attachment $attachment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attachment  $attachment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attachment $attachment)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\FileController;

use App\Models\Advertisement;

class AdvertisementController extends ApiController
 {
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function index(){
        $data = Advertisement::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched');
        return $this->successResponse( null,$data, $msg, 200 );
    }

    public function getById( Request $request ){
        $advertisement = Advertisement::where( 'id', $request->id )->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched');
        return $this->successResponse( $advertisement,null, $msg, 200 );
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function create() {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function store( Request $request ){
        $file= $request->file('file');
        $uploaded =FileController::uploadFile($file);
        $advertisement = Advertisement::create( [
            'image' => $uploaded->getData()->file_url,
            'start_date'=>$request->start_date,
            'expiry_date'=>$request->expiry_date
        ] );
        $msg=\Lang::get('messages.AddedSuccessfully');
        return $this->successResponse( $advertisement,null, $msg, 200 );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Advertisement  $advertisement
    * @return \Illuminate\Http\Response
    */

    public function show( Advertisement $advertisement )
 {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Advertisement  $advertisement
    * @return \Illuminate\Http\Response
    */

    public function edit( Advertisement $advertisement )
 {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Advertisement  $advertisement
    * @return \Illuminate\Http\Response
    */

    public function update( Request $request ) {
        // $request->validate( [
        //     'image' => 'required'
        // ] );
        $advertisement = Advertisement::find( $request->id );
        if($request->hasFile('file')){
            $file= $request->file('file');
            $imgURL=$advertisement->image;
            $fileName=FileController::findFileName($imgURL);
            $deleteFile=FileController::deleteFile($fileName);
            $uploadNew=FileController::uploadFile($file);
            $advertisement->image = $uploadNew->getData()->file_url;}
        else{
            $advertisement->image=$advertisement->image;
        }  
        $advertisement->start_date=$request->start_date;
        $advertisement->expiry_date=$request->expiry_date;
        $advertisement->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully');
        return $this->successResponse( $advertisement,null, 'Updated Successfully ', 200 );
    }

    public function deleteById( $id )
 {
        $advertisement = Advertisement::find( $id );
        if ( is_null( $advertisement ) ) {
            $msg=\Lang::get('messages.ObjectNotFound');
            return  $this->errorResponse( $msg, null );
        } else {
            $res = $advertisement->delete();
            if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully');
                return $this->successResponse( $advertisement,null, $msg, 200 );
            } else {
                $msg=\Lang::get('messages.DeletingFailed');
                return  $this->errorResponse( $msg, null );
            }
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Advertisement  $advertisement
    * @return \Illuminate\Http\Response
    */

    public function destroy( Advertisement $advertisement )
 {
        $advertisement->delete();
        $msg=\Lang::get('messages.DeletedSuccessfully');
        return $this->successResponse( $advertisement,null, $msg, 200 );
    }
}

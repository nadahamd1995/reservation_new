<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends ApiController
{
    /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

   public function index()
{
       $data = Comment::all();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse( null,$data, $msg, 200 );
   }

   public function getById( Request $request )
{
       $comment = Comment::where( 'id', $request->id )->first();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse( $comment,null, $msg, 200 );
   }

   /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */

   public function create()
{
       //
   }

   /**
   * Store a newly created resource in storage.
   *
   * @param  \App\Http\Requests\Request  $request
   * @return \Illuminate\Http\Response
   */

   public function store( Request $request )
{
       $comment = Comment::create( [
        'content' =>$request->content,
        'center_id'=>$request->center_id,
        'user_id'=>$request->user_id
       ] );
       $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
       return $this->successResponse( $comment,null, $msg, 200 );
   }

   /**
   * Display the specified resource.
   *
   * @param  \App\Models\Comment  $comment
   * @return \Illuminate\Http\Response
   */

   public function show( Comment $comment )
{
       //
   }

   /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Comment  $comment
   * @return \Illuminate\Http\Response
   */

   public function edit( Comment $comment )
{
       //
   }

   /**
   * Update the specified resource in storage.
   *
   * @param  \App\Http\Requests\Request  $request
   * @param  \App\Models\Comment  $comment
   * @return \Illuminate\Http\Response
   */

   public function update( Request $request, Comment $comment )
{
       $request->validate( [
           'content' => 'required',
           'center_id' => 'required',
           'user_id'=>'required'
       ] );
       $comment = Comment::find( $request->id );
       $comment->content = $request->content;
       $comment->center_id = $request->center_id;
       $comment->user_id = $request->user_id;
       $comment->save();
       $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
       return $this->successResponse( $comment,null, $msg, 200 );
   }

   public function deleteById( Request $request )
{
       $comment = Comment::find( $request->id );
       if ( is_null( $comment ) ) {
        $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
           return  $this->errorResponse( $msg, null );
       } else {
           $res = $comment->delete();
           if ( $res ) {
            $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
               return $this->successResponse( $comment,null,  $msg, 200 );
           } else {
            $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
               return  $this->errorResponse(  $msg, null );
           }
       }
   }

   /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Comment  $comment
   * @return \Illuminate\Http\Response
   */

   public function destroy( Comment $comment )
{
       $comment->delete();
       $msg=\Lang::get('messages.DeletedSuccessfully');
       return $this->successResponse( $comment,null,  $msg, 200 );
   }
}
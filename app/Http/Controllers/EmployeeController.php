<?php

namespace App\Http\Controllers;
use Lang;
use App\Models\Employee;
use App\Http\Requests\StoreEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;

class EmployeeController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data=Employee::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$data, $msg, 200 );
    }


    public function getById( Request $request )
    {
           $employee = Employee::where( 'id', $request->id )->first();
           $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
           return $this->successResponse( $employee, null,$msg, 200 );
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEmployeeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployeeRequest $request)
    {
        //
        $addEmployee=Employee::create([
            'Arabic_Name' =>$request->Arabic_Name,
            'English_Name'=>$request->English_Name,
            'Phone_number'=>$request->Phone_number
        ]);
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return  $this->successResponse( $addEmployee,null, $msg, 200 );
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEmployeeRequest  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        //
        $request->validate( [
           'Arabic_Name' => 'required',
           'English_Name' => 'required',
           'Phone_Number'=>'required'
       ] );
       $employee = Employee::find( $request->id );
       $employee->Arabic_Name = $request->Arabic_Name;
       $employee->English_Name = $request->English_Name;
       $employee->Phone_number = $request->Phone_number;
       $employee->save();
       $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
       return $this->successResponse( $employee,null,$msg, 200 );
    }


    public function deleteById( Request $request )
    {
           $employee = Employee::find( $request->id );
           if ( is_null( $employee ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
               return  $this->errorResponse( $msg, null );
           } else {
               $res = $employee->delete();
               if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                   return $this->successResponse( $employee,null, $msg, 200 );
               } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                   return  $this->errorResponse( $msg, null );
               }
           }
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }


   

}

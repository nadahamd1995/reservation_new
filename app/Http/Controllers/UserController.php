<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Validator;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Builder;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role=$request->role;
        $roleName=Role::where('name','=',$role);
        //
        $validator = Validator::make($request->all(), [
            'user_name' => 'string|between:2,100',
            'phone_number'=>'required|string|unique:users|between:10,12',
            'password' => 'required|string|confirmed|min:6',
            
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
            
        }

        /** system model =0  that means web 
         * and when system model =1 that means mobile 
         * here we set system model 0 becaues we add user from admin 
         */
        if(!empty($roleName)){ 
            $user = User::create(array_merge(
            $validator->validated(),
            ['password' => bcrypt($request->password),
            'system_model'=>0,
            'is_changed'=>0]
        ));
           $user->assignRole($role);
           $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        //    return $this->successResponse( $user,null, $msg, 200 );}
        return $user;
        }
else{
    $msg=\Lang::get('messages.RoleNotFound',[],$request->lang);
    return  $this->errorResponse( $msg, null );
}

      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::where('id','=',$id)->first();
        $user->delete();
    }
}

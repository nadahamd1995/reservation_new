<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AttachmentController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\UserController;
use App\Models\Center;
use App\Models\Menu;
use App\Models\Working_Hours;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Lang;

class CenterController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $data = Center::all();
        $msg = \Lang::get('messages.DataSuccessfullyFetched', [], $request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }

    public function getById(Request $request)
    {

        $center = Center::where('id', $request->id)->with('category')
            ->with('user')
            ->with('propertyCenter')
            ->with('workingHours')
            ->with('attachments')
            ->first();

        if (!empty($center['workingHours']) || !is_null($center['workingHours'])) {
            $centerWorking = $center['workingHours'];
            if (count($centerWorking) != 0) {

                foreach ($centerWorking as $working) {
                    $fromDateNew = Carbon::parse($working->fromDate)->format('g:i a');
                    $toDateNew = Carbon::parse($working->toDate)->format('g:i a');
                    $working['fromDate'] = $fromDateNew;
                    $working['toDate'] = $toDateNew;
                }
            }} 

    
        // error_log('9999999');
        // $mytime = Carbon::now(new \DateTimeZone('Asia/Damascus'));
        // $mytimeX=$mytime->toTimeString();
        // $dayName = $mytime->format('l');
        // $dayId = WeekDaysController::getByName($dayName);
        // $work_hour = Working_Hours::where('center_id', '=', $center->id)->where('week_days_id', '=', $dayId)->first();
        // if (!empty($work_hour) && !is_null($work_hour)) {
        //     if (!empty($center['workingHours'])) {
                
        //         $temp=$work_hour->fromDate;
        //         error_log($temp);
        //         // if ($center->workingHours[0]->fromDate >= $mytimeX && $mytimeX <= $center->workingHours[0]->toDate)
        //         if ($work_hour['fromDate'] <= $mytimeX && $mytimeX <= $work_hour['toDate'])
        //          {
        //             error_log('eeeeee');
        //             $center->status = 1;
        //             $center->save();
        //         } else {
        //             $center->status = 0;
        //             $center->save();
        //         }

        //     } else {
        //         $center->status = 0;
        //         $center->save();
        //     }
        // } else {
        //     error_log('yesssss');
        //     $center->status = 0;
        //     $center->save();
        // }

        $menu = Menu::where('center_id', '=', $center->id)->first();
        $center['menu'] = $menu;
        $msg = \Lang::get('messages.DataSuccessfullyFetched', [], $request->lang);
        return $this->successResponse($center, null, $msg, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $data = [];
        $request->role = 'Center Admin';
        $user = UserController::store($request);
        error_log($user);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $uploaded = FileController::uploadFile($file);
            $Logo = $uploaded->getData()->file_url;
        } else {
            $Logo = null;
        }

        if ($request->hasFile('cover_photo')) {
            $coverPhoto = $request->file('cover_photo');
            $uploadCover = FileController::uploadFile($coverPhoto);
            $cover_photo = $uploaded->getData()->file_url;
        } else {
            $cover_photo = null;
        }

        $center = Center::create([
            'name' => $request->name,
            'arabic_name' => $request->arabic_name,
            'Logo' => $Logo,
            'category_id' => $request->category_id,
            'totalAvg' => "0.0",
            'address' => $request->address,
            'arabic_description' => $request->arabic_description,
            'english_description' => $request->english_description,
            'tables_number' => $request->tables_number,
            'center_phone' => $request->center_phone,
            'center_mobile' => $request->center_mobile,
            'cover_photo' => $cover_photo,
            'user_id' => $user->id,
        ]);

        if (empty($center) && is_null($center)) {
            $user = UserController::destroy($user->id);
        }

        $property_center = PropertyCenterController::addList($center, $request);

        if ($request->hasFile('menu')) {
            $menu = MenuController::store($request, $center);}

        if ($request->hasFile('attachments')) {
            $attachment = AttachmentController::addList($request, $center);}

        $msg = \Lang::get('messages.AddedSuccessfully', [], $request->lang);
        return $this->successResponse($center, null, $msg, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */

    public function show(Center $center)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */

    public function edit(Center $center)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        // $request->validate([
        //     'name' => 'required',
        //     'arabic_name' => 'required',
        //     'category_id' => 'required',
        //     'address' => 'required',
        //     'tables_number' => 'required',
        //     'arabic_description' => 'required|min:20',
        //     'english_description' => 'required|min:20',
        // ]);

        $center = Center::find($request->$center_id);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $logoURL = $center->Logo;
            $fileName = FileController::findFileName($logoURL);
            $deleteFile = FileController::deleteFile($fileName);
            $uploadNew = FileController::uploadFile($file);
            $center->Logo = $uploadNew->getData()->file_url;}

        if ($raquest->hasFile('cover_photo')) {
            $center->cover_photo = $request->file('cover_photo');
            $cover = $center->cover_photo;
            $fileName = FileController::findFileName($cover);
            $deleteFile = FileController::deleteFile($fileName);
            $uploadNew = FileController::uploadFile($file);
            $center->cover_photo = $uploadNew->getData()->file_url;

        }

        if ($raquest->has('name')) {
            $center->name = $request->name;
        }

        if ($raquest->has('arabic_name')) {
            $center->arabic_name = $request->arabic_name;
        }

        // $center->category_id = $request->category_id;
        if ($raquest->has('address')) {
            $center->address = $request->address;
        }

        if ($raquest->has('arabic_description')) {
            $center->arabic_description = $request->arabic_description;
        }

        if ($raquest->has('english_description')) {
            $center->english_description = $request->english_description;
        }

        if ($raquest->has('tables_number')) {
            $center->tables_number = $request->tables_number;
        }

        if ($raquest->has('center_phone')) {
            $center->center_phone = $request->center_phone;
        }

        if ($raquest->has('center_mobile')) {
            $center->center_mobile = $request->center_mobile;
        }

        $center->save();

        $property_center = PropertyCenterController::updateList($center, $request);

        if ($request->hasFile('menu')) {

            $menu = MenuController::update($request);
        }

        // if($request->hasFile('attachments')){

        //     $attachment = AttachmentController::addList($request, $center);
        // }

        $msg = \Lang::get('messages.UpdatedSuccessfully', [], $request->lang);
        return $this->successResponse($center, null, $msg, 200);
    }

    public function deleteById(Request $request)
    {
        $center = Center::find($request->id);
        if (is_null($center)) {
            $msg = \Lang::get('messages.ObjectNotFound', [], $request->lang);
            return $this->errorResponse($msg, null);
        } else {
            $user = UserController::destroy($center->user_id);
            $res = $center->delete();
            if ($res) {
                $msg = \Lang::get('messages.DeletedSuccessfully', [], $request->lang);
                return $this->successResponse($center, null, $msg, 200);
            } else {
                $msg = \Lang::get('messages.DeletingFailed', [], $request->lang);
                return $this->errorResponse($msg, null);
            }
        }
    }

    public function getByCategoryId(Request $request)
    {
        if ($request->with_rate == 1) {
            $data = Center::where('category_id', $request->category_id)->with('rates')
                ->orderBy('totalAvg', 'desc')->limit(1)->get();
        } else { $data = Center::where('category_id', $request->category_id)
                ->with('rates')
                ->get();}
        $msg = \Lang::get('messages.DataSuccessfullyFetched', [], $request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }

    public function dynamicSearch(Request $request)
    {
        $properties = json_decode($request->get('filters'));
        $countProperties = count($properties);
        $i = 0;
        error_log('count ' . $countProperties);
        $query = \DB::table('centers');
        foreach ($properties as $property) {
            // for($i=0 ;$i<$countProperties;$i++){
            $i++;
            $query->join('property__centers as pc' . $i, function ($join) use ($property, $i) {
                $join->on('centers.id', '=', 'pc' . $i . '.center_id')
                    ->where('pc' . $i . '.property_id', '=', $property);
            });
        }
        $data = $query->get();
        $msg = \Lang::get('messages.DataSuccessfullyFetched', [], $request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */

    public function destroy(Center $center)
    {
        $center->delete();
        $msg = \Lang::get('messages.DeletedSuccessfully');
        return $this->successResponse($center, null, $msg, 200);
    }

    public function searchByName(Request $request)
    {
        if ($request->has('name')) {
            if (empty($request->name)) {

                $data = Center::where('centers.category_id', '=', $request->category_id)
                    ->get();
            } else {
                $data = Center::where('centers.name', 'LIKE', '%' . $request->name . '%')
                    ->orWhere('centers.arabic_name', 'LIKE', '%' . $request->name . '%')
                    ->where('centers.category_id', '=', $request->category_id)
                    ->get();
            }
        } else {
            $data = Center::where('centers.category_id', '=', $request->category_id)
                ->get();
        }

        $msg = \Lang::get('messages.DataSuccessfullyFetched', [], $request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }
}

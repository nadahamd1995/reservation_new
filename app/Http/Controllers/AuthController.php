<?php
namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
// use App\Enums\SystemModelEnum;

// use ApiResponser;
class AuthController extends ApiController
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6',
            'user_name'=> 'string|unique:users|between:2,100',
            'phone_number'=>'string'
        ]);
        if ($validator->fails()) {
            // return response()->json($validator->errors(), 422);
           return $this->errorResponse($validator->errors(), 422);
        }
        if (! $token = auth()->attempt($validator->validated())) {
            $msg=\Lang::get('messages.Unauthorized',[],$request->lang);
            return  $this->errorResponse($msg, 401);
            
        }
        // return $this->createNewToken($token);
        $msg=\Lang::get('messages.UserSuccessfullySignedIn',[],$request->lang);
        return $this->successResponse($this->createNewToken($token),null,$msg, 200);
    }
    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|string|unique:users|between:2,100',
            'phone_number'=>'required|string|unique:users|between:10,12',
            'password' => 'required|string|confirmed|min:6',
        ]);
        if($validator->fails()){
            // return response()->json($validator->errors()->toJson(), 400);
            return $this->errorResponse($validator->errors(), 400);
        }
        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password),
                    'is_changed'=>1,
                    'system_model'=>1]
                ));
         $user->assignRole('User');
         
        //  $user->system_model=\App\Enums\SystemModelEnum::Mobile;
        // return response()->json([
        //     'message' => 'User successfully registered',
        //     'user' => $user
        // ], 201);
        $msg=\Lang::get('messages.UserSuccessfullyRegistered',[],$request->lang);
        return $this->successResponse($user,null,$msg, 201);
    }


    public function changePassword(Request $request){

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            $msg=\Lang::get('messages.currentPasswordError',[],$request->lang);
            return $this-> errorResponse($msg,null);
        }

        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            $msg=\Lang::get('messages.newPasswordError',[],$request->lang);
            //Current password and new password are same
            return $this-> errorResponse($msg,null);
        }

        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|string|min:6',
            'confirm_new_password' => ['same:new_password'],
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->is_changed=1;
        $user->save();
        $msg=\Lang::get('messages.PasswordChangedSuccessfully',[],$request->lang);
        return $this->successResponse($user,null,$msg,null);

    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();
        $msg=\Lang::get('messages.UserSuccessfullySignedOut',[],$request->lang);
        return $this->successResponse(null,null,$msg, 200);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }
    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse(auth()->user(),null,$msg, 200);
    }
    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        $data=DB::table('model_has_roles')->where('model_id','=',auth()->user()->id)->get();
        $role=Role::find($data[0]->role_id);
        error_log($role);
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() ,
            'user' => auth()->user(),
            'role'=>$role->name
            
        ]);
    }
}
<?php

namespace App\Http\Controllers;
use Lang;
use App\Models\Rate;
use App\Models\Rate_Property;
use App\Models\Center;
// use App\Http\Controllers\CenterController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class RateController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $requset)
    {
        $data = Rate::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$requset->lang);
        return $this->successResponse(null, $data,$msg, 200);
    }

    public function getById(Request $request)
    {
        $rate = Rate::where('id', $request->id)->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse($rate, null, $msg, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rate = Rate::create([
            'value' => $request->value,
            'rate_property_id' => $request->rate_property_id,
            'user_id' => Auth::id(),
            'center_id' => $request->center_id,
        ]);
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse($rate, null, $msg, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function show(Rate $rate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function edit(Rate $rate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rate $rate)
    {
        $request->validate([
            'value' => 'required',
            'rate_property_id' => 'required',
            'center_id' => 'required',
        ]);
        $rate = Rate::find($request->id);
        $rate->value = $request->value;
        $rate->center_id = $request->center_id;
        $rate->user_id =  Auth::id();
        $rate->rate_property_id = $request->rate_property_id;
        $rate->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse($rate, null, $msg, 200);
    }

    public function deleteById(Request $request)
    {
        $rate = Rate::find($request->id);
        if (is_null($rate)) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
            return $this->errorResponse($msg, null);
        } else {
            $res = $rate->delete();
            if ($res) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                return $this->successResponse($rate, null, $msg, 200);
            } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return $this->errorResponse($msg, null);
            }
        }
    }

    public function getByCenterId(Request $request)
    {
        $data = Rate::join('rate__properties', 'rate__properties.id', '=', 'rates.rate_property_id')
            ->where('center_id', '=', $request->center_id)->get();
            $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rate  $rate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rate $rate)
    {
        $rate->delete();
    }

    public function getRateOfProerety(Request $request)
    {
        $rateProperties = Rate_Property::where('category', '=', $request->category)->get();
        $countrateProperties =count($rateProperties);
        $i = 0;
        $totalAvg=0;
        $sum=0;
        $center_id = $request->center_id;
        $data = []; 
        $total=[];
        error_log($countrateProperties);
        foreach ($rateProperties as $property) {
            $rateByProperties=Rate::where('rate_property_id','=',$property->id)->get();
            $countRateByProperties=count($rateByProperties);
            $value = $property->value;
            $id = $property->id;
            if ($i > $countrateProperties) {
                
                     break;} 

            if($i > $countRateByProperties ){

                break;
            }
            
            else {
                $query = Rate::select(\DB::raw('(case when rates.rate_property_id=' . $id . ' and rates.center_id=' . $center_id . ' then AVG(rates.value)  end ) as  Avg_' . $value))->groupBy('rates.rate_property_id', 'rates.center_id');
                $query=$query->get();
                $data[$value]=strval($query[$i]['Avg_'.$value]);
                $total=$query[$i]['Avg_'.$value];
                $sum=$sum+$total;
                $rate_property=Rate_Property::where('id','=',$id)->first();


                /**create totalRate variable to set avg value for each property 
                 * and this value will set in totalRate column in rate_property table */
                $totalRate=$query[$i]['Avg_'.$value];

                /** strval is function to convert from int to string */
                $totalRateX=strval($totalRate);
                $rate_property->total_rate=$totalRateX;
                $rate_property->save();
                
                
            }
            $i++;
           

        }
        $totalAvg=strval($sum/$i);
        error_log($totalAvg);
        $data['total']=$totalAvg;

        /** count the number of raters based on  stars count in rate table  */
        $count=Rate::select(\DB::raw('SUM(IF(value=5,1,0)) as Rate_five_star,SUM(IF(value=4,1,0)) as Rate_four_star,SUM(IF(value=3,1,0)) as Rate_three_star,SUM(IF(value=2,1,0)) as Rate_two_star,SUM(IF(value=1,1,0)) as Rate_one_star'))->get();
        $data['Rate_five_star']=$count[0]['Rate_five_star'];
        $data['Rate_four_star']=$count[0]['Rate_four_star'];
        $data['Rate_three_star']=$count[0]['Rate_three_star'];
        $data['Rate_two_star']=$count[0]['Rate_two_star'];
        $data['Rate_one_star']=$count[0]['Rate_one_star'];
        
        /** set total avg in center table to get the centers that have  most high rate  */
        $center= Center::where( 'id', $request->center_id )->first();
        $center->totalAvg=$totalAvg;
        $center->save();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse($data, null, $msg, 200);

    }

    

}

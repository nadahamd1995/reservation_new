<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Rate_Property;

class RatePropertyController extends ApiController
{
       //
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
      /**
     * Create a new RatePropertyController instance.
     *
     * @return void
     */
   

    public function index(Request $request)
 {
        $data = Rate_Property::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$data, $msg, 200 );
    }

    public function getById( Request $request )
 {
        $rateProperty = Rate_Property::where( 'id', $request->id )->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( $rateProperty,null, $msg, 200 );
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function create()
 {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function store( Request $request )
 {
        $rateProperty = Rate_Property::create( [
            'value'=>$request->value,
            'arabic_value'=>$request->arabic_value,
            'total_rate'=>"0.0",
            'category' =>$request->category
        ] );
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse( $rateProperty,null, $msg, 200 );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Rate_Property  $rateProperty
    * @return \Illuminate\Http\Response
    */

    public function show( Rate_Property $rateProperty )
 {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Rate_Property  $rateProperty
    * @return \Illuminate\Http\Response
    */

    public function edit( Rate_Property $rateProperty )
 {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Rate_Property  $rateProperty
    * @return \Illuminate\Http\Response
    */

    public function update( Request $request, Rate_Property $rateProperty )
 {
        $request->validate( [           
            'value'=>'required',
            'arabic_value'=>'required',
            'category' =>'required'
             ] );
        $rateProperty = Rate_Property::find( $request->id );
        $rateProperty->value = $request->value;
        $rateProperty->arabic_value = $request->arabic_value;
        $rateProperty->category = $request->category;
        $rateProperty->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse( $rateProperty,null, $msg, 200 );
    }

    public function deleteById(Request $request )
 {
        // $id = ;
        $rateProperty = Rate_Property::find( $request->id );
        // $advertisement = Advertisement::find( $request->id );
        if ( is_null( $rateProperty ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
            return  $this->errorResponse( $msg, null );
        } else {
            $res = $rateProperty->delete();
            if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                return $this->successResponse( $rateProperty,null, $msg, 200 );
            } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return  $this->errorResponse( $msg, null );
            }
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Rate_Property  $rateProperty
    * @return \Illuminate\Http\Response
    */

    public function destroy( Rate_Property $rateProperty )
 {
        $rateProperty->delete();
        $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
        return $this->successResponse( $rateProperty,null, $msg, 200 );
    }
}

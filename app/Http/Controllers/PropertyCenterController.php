<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Property_Center;

class PropertyCenterController extends ApiController
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index(Request $request)
   {
       $data = Property_Center::all();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse( null,$data, $msg, 200 );
   }


   public function getById( Request $request )
   {
          $property = Property_Center::where( 'id', $request->id )->first();
          $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
          return $this->successResponse( $property,null, $msg, 200 );
      }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $property = Property_Center::create( [
           'value' => $request->value,
           'center_id'  => $request->center_id,
           'property_id'  => $request->property_id
       ] );
       $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
       return $property;
    //    return $this->successResponse( $property,null,$msg, 200 );
   }

   public function addList($center,$request){
   
    $properties=json_decode($request->get('properties'));
    error_log($request->get('properties'));
    foreach($properties as $property){
        $property = Property_Center::create( [
            'value' => null,
            'center_id'  => $center->id,
            'property_id'  => $property
        ] );
    }
    
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Models\Property_Center  $property
    * @return \Illuminate\Http\Response
    */
   public function show(Property_Center $property)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Property_Center  $property
    * @return \Illuminate\Http\Response
    */
   public function edit(Property_Center $property)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Property_Center  $property
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Property_Center $property)
   {
       $request->validate( [
           'value' => 'required',
           'center_id'  =>'required',
           'property_id'  => 'required'
           
       ] );
       $property = Property_Center::find( $request->id );
       $property->value = $request->value;
       $property->save();
       $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
       return $this->successResponse( $property,null, $msg, 200 );
   }

   public function updateList($center,$request){


    $propertiesCenter=json_decode($request->get('properties'));
    foreach($propertiesCenter as $property_center){
        $propertyCenter = Property_Center::find( $property_center->id );
        $propertyCenter->property_id=$property_center->property_id;
        $propCenter->save();
    }

    // $propertiesByCenter = Property_Center::where('center_id','=',$center->id); 
    //     foreach($propertiesByCenter as $propCenter){
    //         $propCenter->property_id=$property;


    //     }

    
   }

   /**/

   public function getByCenter(Request $request){
 
    $date=Property_Center::join('centers','property__centers.center_id','=','centers.id')
                          ->join('properties','property__centers.property_id','=','properties.id')
                          ->where('center_id','=',$request->center_id)->get();
                          $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
    return $this->successResponse( null,$date, $msg, 200 );

   }


   public function deleteById(Request $request )
   {
          $property = Property_Center::find( $request->id );
          if ( is_null( $property ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
              return  $this->errorResponse( $msg, null );
          } else {
              $res = $property->delete();
              if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                  return $this->successResponse( $property,null, $msg, 200 );
              } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                  return  $this->errorResponse( $msg, null );
              }
          }
      }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Property_Center  $property
    * @return \Illuminate\Http\Response
    */
   public function destroy(Property_Center $property)
   {
       $property->delete();
   }
}

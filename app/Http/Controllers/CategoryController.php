<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\FileController;

class CategoryController extends ApiController
{
    /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */

   public function index(Request $request){
       $data = Category::all();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse(null, $data, $msg, 200 );
   }

   public function getById( Request $request )
{
       $category = Category::where( 'id', $request->id )->first();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse( $category, null,$msg, 200 );
   }

   /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */

   public function create()
{
       //
   }

   /**
   * Store a newly created resource in storage.
   *
   * @param  \App\Http\Requests\Request  $request
   * @return \Illuminate\Http\Response
   */

   public function store( Request $request )
{

    $file=$request->file('file');
    $uploaded =FileController::uploadFile($file);
       $category = Category::create( [
           'name' => $request->name,
           'english_name'=>$request->english_name,
           'arabic_name'=> $request->arabic_name,
           'image'=>$uploaded->getData()->file_url

       ] );
       $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
       return $this->successResponse( $category,null, $msg, 200,[],$request->lang );
   }

   /**
   * Display the specified resource.
   *
   * @param  \App\Models\Category  $category
   * @return \Illuminate\Http\Response
   */

   public function show( Category $category )
{
       //
   }

   /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Category  $category
   * @return \Illuminate\Http\Response
   */

   public function edit( Category $category )
{
       //
   }

   /**
   * Update the specified resource in storage.
   *
   * @param  \App\Http\Requests\Request  $request
   * @param  \App\Models\Category  $category
   * @return \Illuminate\Http\Response
   */

   public function update( Request $request, Category $category )
{
       $request->validate( [
        //    'name' => 'required',
        //    'arabic_name'=>'required',
        //    'english_name' =>'required',
        //    'image'=>'required'
       ] );

       $file=$request->file('file');
       $category = Category::find( $request->id );
       $imgURL=$category->image;
       $fileName=FileController::findFileName($imgURL);
       $deleteFile=FileController::deleteFile($fileName);
       $uploadNew=FileController::uploadFile($file);
       $category->name = $request->name;
       $category->arabic_name=$request->arabic_name;
       $category->image=$uploadNew->getData()->file_url;
       $category->save();
       $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
       return $this->successResponse( $category, null,$msg, 200 );
   }

   public function deleteById( Request $request )
{
       $category = Category::find( $request->id );
       if ( is_null( $category ) ) {
        $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
           return  $this->errorResponse( $msg, null );
       } else {
           $res = $category->delete();
           if ( $res ) {
            $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
               return $this->successResponse( $category,null, $msg, 200 );
           } else {
            $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
               return  $this->errorResponse( $msg, null );
           }
       }
   }

   /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Category  $category
   * @return \Illuminate\Http\Response
   */

   public function destroy( Category $category )
{
       $category->delete();
       $msg=\Lang::get('messages.DeletedSuccessfully');
       return $this->successResponse( $category,null, $msg, 200 );
   }
}

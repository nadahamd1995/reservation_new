<?php

namespace App\Http\Controllers;
use Lang;
use App\Models\Category;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\FileController;

class ServiceController extends ApiController
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $data = Service::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }

    public function getById(Request $request)
    {
        $service = Service::where('id', $request->id)->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse($service, null, $msg, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $img=$request->file('img');
        $uploaded =FileController::uploadFile($img);
        $service = Service::create([
            'name' => $request->name,
            'arabic_name'=>$request->arabic_name,
            'duration' => $request->duration,
            'img'=>$uploaded->getData()->file_url,
            'center_id' => $request->center_id,
            'propertyCenter_id' => $request->propertyCenter_id,
        ]);
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse($service, null, $msg, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */

    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */

    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Service $service)
    {
        $request->validate([
            'name' => 'required',
            'arabic_name'=>'required',
            'duration' => 'required',
            'img'=> 'required',
            'center_id' => 'required',
            'propertyCenter_id' => 'required',
        ]);
        $service = Service::find($request->id);
        $imgURL=$service->img;
        $fileName=FileController::findFileName($imgURL);
        $deleteFile=FileController::deleteFile($fileName);
        $uploadNew=FileController::uploadFile($request);
        $service->name = $request->name;
        $service->arabic_name = $request->arabic_name;
        $service->duration = $request->duration;
        $service->center_id = $request->center_id;
        $service->img=$uploadNew->getData()->file_url;
        $service->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse($service, null, $msg, 200);
    }

    public function deleteById($id)
    {
        // $id = ;
        $service = Service::find($id);
        // $advertisement = Advertisement::find( $request->id );
        if (is_null($service)) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
            return $this->errorResponse($msg, null);
        } else {
            $res = $service->delete();
            if ($res) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                return $this->successResponse($service, null,$msg, 200);
            } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return $this->errorResponse($msg, null);
            }
        }
    }

    public function getByCenterId(Request $request)
    {
        $data = Service::where('center_id', '=', $request->center_id)->get();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse(null, $data, $msg, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */

    public function destroy(Service $service)
    {
        $service->delete();
        $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
        return $this->successResponse($service, null, $msg, 200);
    }

    /*
    get service by property_center_id
    return list of service
     */
    public function getByProCenterId(Request $request)
    {
        $category = Category::where('type', '=', $request->category_id);
        $categorytype = $category->type;
        if ($categorytype == 'Resturant') {
            $services = Service::join('property__centers', 'services.propertyCenter_id', '=', 'property__centers.id')
                                ->where('propertyCenter_id', '=', $request->propertyCenter_id)->get();
                                $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
            return $this->successResponse(null, $services, $msg, 200);} 
            
            else {
            $services = Service::join('property__centers', 'services.propertyCenter_id', '=', 'property__centers.id')
                ->join('emp_services', 'emp_services.service_id', '=', 'services.id')
                ->where('propertyCenter_id', '=', $request->propertyCenter_id)
                ->where('emp_services.employee_id', '=', $request->employee_id)->get();
                $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
            return $this->successResponse(null, $services, $msg, 200);
        }

    }
}

<?php

namespace App\Http\Controllers;
use Lang;
use App\Models\EmpService;
use App\Http\Requests\StoreEmpServiceRequest;
use App\Http\Requests\UpdateEmpServiceRequest;

class EmpServiceController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data=EmpService::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$data, $msg, 200 );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreEmpServiceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmpServiceRequest $request)
    {
        //
        $addEmpService=EmpService::create([
            'center_id'=>$request->center_id,
            'service_id'=>$request->service_id,
            'employee_id'=>$request->employee_id
        ]);
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse( $addEmpService,null,$msg, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmpService  $empService
     * @return \Illuminate\Http\Response
     */
    public function show(EmpService $empService)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmpService  $empService
     * @return \Illuminate\Http\Response
     */
    public function edit(EmpService $empService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateEmpServiceRequest  $request
     * @param  \App\Models\EmpService  $empService
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmpServiceRequest $request, EmpService $empService)
    {
        //
    }


    public function deleteById(Request $request )
    {
           $empService = EmpService::find( $request->id );
           if ( is_null( $empService ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
               return  $this->errorResponse( $msg, null );
           } else {
               $res = $empService->delete();
               if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                   return $this->successResponse( $empService,null, $msg, 200 );
               } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                   return  $this->errorResponse( $msg, null );
               }
           }
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmpService  $empService
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmpService $empService)
    {
        //
    }
}

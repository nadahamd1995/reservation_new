<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Working_Hours;
use Carbon\Carbon;

class WorkingHoursController extends ApiController
{
 /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    
 

    public function index(Request $request)
 {
   
        $data = Working_Hours::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$data, $msg, 200 );
    
        
    }

    public function getById( Request $request )
 {
        $workingHours = Working_Hours::where( 'id', $request->id )->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( $workingHours,null, $msg, 200 );
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function create()
 {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function store( Request $request )
 {
    
        $workingHours = Working_Hours::create( [
            'toDate'=>$request->toDate,
            'fromDate'=>$request->fromDate,
            'center_id'=>$request->center_id,
            'week_days_id'=>$request->week_days_id
        ] );
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse( $workingHours,null,$msg, 200 );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Working_Hours  $workingHours
    * @return \Illuminate\Http\Response
    */

    public function show( Working_Hours $workingHours )
 {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Working_Hours  $workingHours
    * @return \Illuminate\Http\Response
    */

    public function edit( Working_Hours $workingHours )
 {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Working_Hours  $workingHours
    * @return \Illuminate\Http\Response
    */

    public function update( Request $request, Working_Hours $workingHours )
 {
        $request->validate( [           
        'fromDate'=> 'required',
        'toDate' => 'required',
        'center_id'=> 'required',
        'week_days_id'=> 'required'
    
        ] );
        $workingHours = Working_Hours::find( $request->id );
        $workingHours->fromDate = $request->fromDate;
        $workingHours->toDate = $request->toDate;
        $workingHours->center_id = $request->center_id;
        $workingHours->week_days_id = $request->week_days_id;
        $workingHours->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse( $workingHours,null, $msg, 200 );
    }

    public function deleteById(Request $request )
 {
        // $id = ;
        $workingHours = Working_Hours::find($request->id );
        // $advertisement = Advertisement::find( $request->id );
        if ( is_null( $workingHours ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
            return  $this->errorResponse( $msg, null );
        } else {
            $res = $workingHours->delete();
            if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                return $this->successResponse( $workingHours,null, $msg, 200 );
            } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return  $this->errorResponse( $msg, null );
            }
        }
    }

    /*

    */ 
    public function getByCenter(Request $request )
    {
        $workingHours=Working_Hours::join('week__days', 'week__days.id', '=', 'working__hours.week_days_id')
        ->where('center_id','=', $request->center_id)->get();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$workingHours, $msg, 200 );


    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Working_Hours  $workingHours
    * @return \Illuminate\Http\Response
    */

    public function destroy( Working_Hours $workingHours )
 {
        $workingHours->delete();
        $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
        return $this->successResponse( $workingHours,null,$msg, 200 );
    }}

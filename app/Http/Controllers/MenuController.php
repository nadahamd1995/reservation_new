<?php

namespace App\Http\Controllers;
use Lang;
use App\Http\Controllers\FileController;

use Illuminate\Http\Request;
use App\Models\Menu;



class MenuController extends ApiController
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Menu::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse(null, $data, $msg, 200 );
    }


    public function getById( Request $request )
    {
           $menu = Menu::where( 'id', $request->id )->first();
           $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
           return $this->successResponse( $menu,null, $msg, 200 );
       }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$center)
    {

        $menu = $request->file('menu');
        $uploaded = FileController::uploadFile($menu);
        $menu = Menu::create( [
            'name' => $uploaded->getData()->fileName,
            'path'=> $uploaded->getData()->file_url,
            'center_id'=>$center->id
                ] );
        
        return  $menu;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Request  $request
     * @param  \App\Models\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // $request->validate( [
        //     'file' => 'required',
        //     'center_id'=>'required'
        // ] );
        $menu = Menu::where('center_id','=',$request->center_id)->get();
        $file=$menu->path;
        $fileName=FileController::findFileName($file);
        $deleteFile=FileController::deleteFile($fileName);
        $uploadNew=FileController::uploadFile($file);
        $menu->file = $uploadNew->getData()->file_url;
        $menu->center_id = $request->center_id;
        $menu->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse( $menu,null, $msg, 200 );
    }


    public function deleteById( Request $request )
    {

           $menu = Menu::find( $request->id );
           if ( is_null( $menu ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
               return  $this->errorResponse($msg, null );
           } else {
            $deleted=FileController::deleteFile($menu->name);
            if($deleted){
                $res = $menu->delete();
                    if ( $res ) {
                        $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                        return $this->successResponse( $menu, null,$msg, 200 );
                    } else {
                        $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                        return  $this->errorResponse($msg, null );
                    }}
               else{
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return  $this->errorResponse( $msg, null );}
            }
       }

       public function getByCenterId(Request $request )
       {
           $data = Menu::where('center_id','=', $request->center_id)->get();
           $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
           return $this->successResponse( $data,null, $msg, 200 );
       }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $Menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
    }

}

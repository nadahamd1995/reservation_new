<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Property;

class PropertyController extends ApiController
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function index(Request $request)
   {
       $data = Property::all();
       $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
       return $this->successResponse( null,$data, $msg, 200 );
   }


   public function getById( Request $request )
   {
          $property = Property::where( 'id', $request->id )->first();
          $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
          return $this->successResponse( $property,null, $msg, 200 );
      }

      
   public function getByCategory( Request $request )
   {
          $property = Property::where( 'category_id', $request->category_id )->get();
          $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
          return $this->successResponse( null,$property, $msg, 200 );
      }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function create()
   {
       //
   }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function store(Request $request)
   {
       $property = Property::create( [
           'value' => $request->value,
           'arabic_value'=> $request->arabic_value,
           'property_name' =>$request->property_name,
           'category_id' =>$request->category_id
       ] );
       $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
       return $this->successResponse( $property,null,$msg, 200 );
   }

   /**
    * Display the specified resource.
    *
    * @param  \App\Models\Property  $property
    * @return \Illuminate\Http\Response
    */
   public function show(Property $property)
   {
       //
   }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Property  $property
    * @return \Illuminate\Http\Response
    */
   public function edit(Property $property)
   {
       //
   }

   /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Property  $property
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, Property $property)
   {
       $request->validate( [
           'value' => 'required',
           'arabic_value' => 'required',
           'property_name'=>'required',
           'category_id'=>'category_id'
       ] );
       $property = Property::find( $request->id );
       $property->value = $request->value;
       $property->arabic_value = $request->arabic_value;
       $property->category_id = $request->category_id;
       $property->save();
       $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
       return $this->successResponse( $property,null, $msg, 200 );
   }


   public function deleteById(Request $request )
   {
          $property = Property::find( $request->id );
          if ( is_null( $property ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
              return  $this->errorResponse($msg, null );
          } else {
              $res = $property->delete();
              if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                  return $this->successResponse( $property,null, $msg, 200 );
              } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                  return  $this->errorResponse($msg, null );
              }
          }
      }

   /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Property  $property
    * @return \Illuminate\Http\Response
    */
   public function destroy(Property $property)
   {
       $property->delete();
   }
}

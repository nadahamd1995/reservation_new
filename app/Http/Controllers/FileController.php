<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Str;
use File;

class FileController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FileModel  $fileModel
     * @return \Illuminate\Http\Response
     */
    public function show(FileModel $fileModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FileModel  $fileModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileModel $fileModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FileModel  $fileModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileModel $fileModel)
    {
        //
    }

    public function uploadFile($file){

        // Validation
        // $request->validate([
        //   'file' => 'required|mimes:png,jpg,jpeg,csv,txt,pdf|max:2048'
        // ]); 
      
        $fileName=time().'_'.$file->getClientOriginalName();
        $file->move(public_path("/").'files/',$fileName);
        $file_url=url('files',$fileName);
        return response()->json([
            'fileName' =>  $fileName,
            'file_url' => $file_url
          ]);
        
    }

    public function deleteFile($file_name){
        if(File::exists(public_path('files/'.$file_name))){
            File::delete(public_path('files/'.$file_name));
        }else{
            return  false;
        }
        return true;
    }    

    public function findFileName($file_url){
       $stringLength = Str::length($file_url);
       $findme='files/';
       $pos=strpos($file_url,$findme);
       $fileName=Str::substr($file_url, $pos+6,$stringLength-1);
       return $fileName;
    }
}

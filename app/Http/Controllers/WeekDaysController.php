<?php

namespace App\Http\Controllers;
use Lang;
use Illuminate\Http\Request;
use App\Models\Week_Days;

class WeekDaysController extends ApiController
{
    //
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

      
 
    public function index(Request $request)
 {
        $data = Week_Days::all();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( null,$data, $msg, 200 );
    }

    public function getById( Request $request )
 {
        $weekDays = Week_Days::where( 'id', $request->id )->first();
        $msg=\Lang::get('messages.DataSuccessfullyFetched',[],$request->lang);
        return $this->successResponse( $weekDays,null, $msg, 200 );
    }

    public function getByName($name){
  
        $dayName=Week_Days::where('week__days.name','LIKE', '%' . $name . '%')->get();
        return $dayName[0]['id'];
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function create()
 {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @return \Illuminate\Http\Response
    */

    public function store( Request $request )
 {
        $weekDays = Week_Days::create( [
            'name'=>$request->name,
            'arabic_name'=>$request->arabic_name
        ] );
        $msg=\Lang::get('messages.AddedSuccessfully',[],$request->lang);
        return $this->successResponse( $weekDays,null, $msg, 200 );
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\Models\Week_Days  $weekDays
    * @return \Illuminate\Http\Response
    */

    public function show( Week_Days $weekDays )
 {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\Models\Week_Days  $weekDays
    * @return \Illuminate\Http\Response
    */

    public function edit( Week_Days $weekDays )
 {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \App\Http\Requests\Request  $request
    * @param  \App\Models\Week_Days  $weekDays
    * @return \Illuminate\Http\Response
    */

    public function update( Request $request, Week_Days $weekDays )
 {
        $request->validate( [           
            'name'=> 'required',
            'arabic_name'=>'required'
        ] );
        $weekDays = Week_Days::find( $request->id );
        $weekDays->name = $request->name;
        $weekDays->arabic_name = $request->arabic_name;
        $weekDays->save();
        $msg=\Lang::get('messages.UpdatedSuccessfully',[],$request->lang);
        return $this->successResponse( $weekDays,null,$msg, 200 );
    }

    public function deleteById(Request $request )
 {
        // $id = ;
        $weekDays = Week_Days::find( $request->id );
        // $advertisement = Advertisement::find( $request->id );
        if ( is_null( $weekDays ) ) {
            $msg=\Lang::get('messages.ObjectNotFound',[],$request->lang);
            return  $this->errorResponse($msg, null );
        } else {
            $res = $weekDays->delete();
            if ( $res ) {
                $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
                return $this->successResponse( $weekDays,null, $msg, 200 );
            } else {
                $msg=\Lang::get('messages.DeletingFailed',[],$request->lang);
                return  $this->errorResponse( $msg, null );
            }
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\Models\Week_Days  $weekDays
    * @return \Illuminate\Http\Response
    */

    public function destroy( Week_Days $weekDays )
 {
        $weekDays->delete();
        $msg=\Lang::get('messages.DeletedSuccessfully',[],$request->lang);
        return $this->successResponse( $weekDays,null, $msg, 200 );
    }
}

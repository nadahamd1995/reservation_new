<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'link',
        'center_id'
    ];

    public function center()
    {
        return $this->belongsTo(Center::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property_Center extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'value' ,
        'center_id',
        'property_id'
    ];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function center()
    {
        return $this->belongsTo(Center::class);
    }
}

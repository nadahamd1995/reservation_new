<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'english_name',
        'arabic_name' ,
        'image'
    ];

    public function center()
    {
        return $this->hasMany(Center::class,'category_id','id');
    }

    public function property()
    {
        return $this->hasMany(Property::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Working_Hours extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'fromDate',
        'toDate' ,
        'center_id',
        'week_days_id',
        'employee_id'
    ];

    public function center()
    {
        return $this->belongsTo(Center::class);
    }

    public function weekDays()
    {
        return $this->belongsTo(Week_Days::class);
    }

    public function Employee(){
        return $this->belongsTo(Employee::class);
    }
}

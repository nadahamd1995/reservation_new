<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate_Property extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'value' ,
        'arabic_value',
        'category',
        'total_rate'
    ];

    public function rates()
    {
        return $this->hasMany(Rate::class);
    }
}

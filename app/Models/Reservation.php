<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'hour',
        'date' ,
        'center_id',
        'user_id',
        'emp_services_id'
    ];
    
    public function center()
    {
        return $this->belongsTo(Center::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
 
    public function emp_services()
    {
        return $this->belongsTo(EmpService::class);
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'path',
        'center_id' 
    ];

    public function center()
    {
        return $this->belongsTo(Center::class);
    }
}

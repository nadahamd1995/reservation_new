<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name' ,
        'arabic_name',
        'Logo',
        'totalAvg',
        'address',
        'center_phone',
        'center_mobile',
        'cover_photo',
        'tables_number',
        'status',
        'arabic_description',
        'english_description',
        'user_id',
        'category_id',
    ];

    // protected $casts = ["totalAvg" => 'float'];
       
    

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function menu()
    {
        return $this->hasOne(Menu::class);
    }

    public function propertyCenter()
    {
        return $this->hasMany(Property_Center::class)->with('property');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function workingHours()
    {
        return $this->hasMany(Working_Hours::class)->with('weekDays');
    }

    public function rates()
    {
        return $this->hasMany(Rate::class)->with('rateProperty');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function emp_services(){
        return $this->hasMany(EmpService::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function attachments(){
        return $this->hasMany(Attachment::class);
    }
    

}

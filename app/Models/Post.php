<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'content' ,
        'image',
        'date',
        'tag',
        'center_id',
        'creator_id'
    ];

    public function center()
    {
        return $this->belongsTo(Center::class);
    }
}

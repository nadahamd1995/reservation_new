<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name' ,
        'arabic_name',
        'duration',
        'img',
        'center_id',
        'propertyCenter_id'
    ];

    public function category()
    {
        return $this->belongsTo(Center::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function emp_services(){
        return $this->hasMany(EmpService::class);
    }
}
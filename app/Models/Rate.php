<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'value' ,
        'center_id',
        'user_id',
        'rate_property_id'
    ];

    public function rateProperty()
    {
        return $this->belongsTo(Rate_Property::class);
    }

    public function center()
    {
        return $this->belongsTo(Center::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

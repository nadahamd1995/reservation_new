<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'Arabic_Name' ,
        'English_Name',
        'Phone_number'
    ];

    public function emp_services(){
        return $this->hasMany(EmpService::class);
    }

    public function working_hours(){
        return $this->hasMany(Working_Hours::class);
    }
}

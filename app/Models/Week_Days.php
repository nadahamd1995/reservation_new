<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Week_Days extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name' ,
        'arabic_name'
    ];

    public function workingHours()
    {
        return $this->hasMany(Working_Hours::class);
    }
}

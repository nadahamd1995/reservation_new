<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmpService extends Model
{
    use HasFactory;

    protected $fillable=[
        'id',
        'center_id',
        'service_id',
        'employee_id',
    ];

    public function Center(){
        return $this->belongsTo(Center::class);
    }

    public function Service(){
     return $this->belongsTo(Service::class);
    }

    public function Employee(){
        return $this->belongsTo(Employee::class);
    }
}

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // General msgs
    'AddedSuccessfully' => 'Added Successfully',
    'UpdatedSuccessfully' =>'Updated Successfully',
    'DataSuccessfullyFetched'=>'Data successfully fetched',
    'DeletedSuccessfully'=>'Deleted Successfully',
    
    // General Errors
    'ObjectNotFound'=>'Object Not Found',
    'DeletingFailed'=>'Deleting Failed',
    'NoPermission'=>'You do not have permission',
    'RoleNotFound'=>'Role not found',
    

        // Auth Controller msgs
        'PasswordChangedSuccessfully'=> 'Password changed successfully !',
        'UserSuccessfullyRegistered' =>'User successfully registered',
        'UserSuccessfullySignedIn' =>'User successfully signed in',
        'UserSuccessfullySignedOut'=>'User successfully signed out',
    
    // ERRORS

        // Auth Controller error msgs
        'newPasswordError' => 'New Password cannot be same as your current password. Please choose a different password.',
        'currentPasswordError' => 'Your current password does not matches with the password you provided. Please try again.',
        'Unauthorized' => 'Unauthorized'

];

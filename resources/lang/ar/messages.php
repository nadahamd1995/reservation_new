<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    // General msgs
    'AddedSuccessfully' => 'تمت الإضافة بنجاح',
    'UpdatedSuccessfully' =>'تم التعديل بنجاح',
    'DataSuccessfullyFetched'=>'تم جلب البيانات بنجاح',
    'DeletedSuccessfully'=>'تم الحذف بنجاح',
    
    // General Errors
    'ObjectNotFound'=>'العنصر غير موجود',
    'DeletingFailed'=>'فشلت عملية الحذف',
    'NoPermission'=>'لا تملك صلاحيات',
    'RoleNotFound'=>'هذا الدور غير موجود',
    

        // Auth Controller msgs
        'PasswordChangedSuccessfully'=> 'تم تغيير كلمة المرور بنجاح',
        'UserSuccessfullyRegistered' =>'تم تسجيل المستخدم بنجاح',
        'UserSuccessfullySignedIn' =>'تم تسجيل الدخول بنجاح',
        'UserSuccessfullySignedOut'=>'تم تسجيل الخروج بنجاح',
    
    // ERRORS

        // Auth Controller error msgs
        'newPasswordError' => 'كلمة المرور الجديدة لا يمكن ان تكون مماثلة للقديمة . يرجى إعادة المحاولة',
        'currentPasswordError' => 'كلمة المرور الحالية لا تتطابق مع التي تم ادخالها. يرجى إعادة المحاولة',
        'Unauthorized' => 'خطأ في اسم المستخدم او كلمة المرور'

];


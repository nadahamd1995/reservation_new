<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centers', function (Blueprint $table) {
            $table->id();
            $table->string('Logo')->nullable();
            $table->string('name')->nullable();
            $table->string('arabic_name')->nullable();
            $table->string('address')->nullable();
            $table->string('totalAvg');
            $table->string('arabice_description')->nullable();
            $table->string('english_description')->nullable();
            $table->integer('tables_number')->nullable();
            $table->integer('status')->default(1);
            $table->string('center_phone')->nullable();
            $table->string('center_mobile')->nullable();
            $table->string('cover_photo')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centers');
    }
}

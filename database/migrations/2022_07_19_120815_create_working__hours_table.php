<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working__hours', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fromDate');
            $table->timestamp('toDate');
            $table->timestamps();
            $table->unsignedBigInteger('center_id');
            $table->unsignedBigInteger('week_days_id');
            $table->unsignedBigInteger('employee_id');
            $table->foreign('center_id')->references('id')->on('centers')->onDelete('cascade');
            $table->foreign('week_days_id')->references('id')->on('week__days')->onDelete('cascade');
            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working__hours');
    }
}

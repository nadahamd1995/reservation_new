<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\AdvertisementController;
use App\Http\Controllers\WorkingHoursController;
use App\Http\Controllers\WeekDaysController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CenterController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PropertyCenterController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\RatePropertyController;
use App\Http\Controllers\RateController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleHasPermissionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmpServiceController;
use App\Http\Controllers\PasswordController;
use App\Http\Controllers\AttachmentController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::get('/user-profile', [AuthController::class, 'userProfile']);      
});
//Advertisement APIs
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'ad'
], function ($router) {
    Route::post('/add', [AdvertisementController::class, 'store']);
    Route::post('/update', [AdvertisementController::class, 'update']); 
    Route::post('/delete/{id}', [AdvertisementController::class, 'deleteById']);      
    Route::post('/delete', [AdvertisementController::class, 'destroy']);            
});

//Advertisement APIs
Route::group([
    'prefix' => 'ad'
], function ($router) {
    Route::get('/all', [AdvertisementController::class, 'index']);
    Route::get('/by-id', [AdvertisementController::class, 'getById']);       
});

//workingHours APIs
Route::group([
    // 'middleware' => ['auth:api','role:Center Admin'],
    'prefix' => 'working-hours'
], function ($router) {
    Route::post('/add', [WorkingHoursController::class, 'store']);
    Route::post('/update', [WorkingHoursController::class, 'update']); 
    Route::post('/delete/{id}', [WorkingHoursController::class, 'deleteById']);      
    Route::post('/delete', [WorkingHoursController::class, 'destroy']);            
});

//workingHours APIs
Route::group([
    'prefix' => 'working-hours'
], function ($router) {
    Route::get('/all', [WorkingHoursController::class, 'index']);
    Route::get('/by-center', [WorkingHoursController::class, 'getByCenter']);
    Route::get('/by-id', [WorkingHoursController::class, 'getById']);         
});

//WeekDays APIs
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'week-days'
], function ($router) {
    Route::post('/add', [WeekDaysController::class, 'store']);
    Route::post('/update', [WeekDaysController::class, 'update']); 
    Route::post('/delete/{id}', [WeekDaysController::class, 'deleteById']);      
    Route::post('/delete', [WeekDaysController::class, 'destroy']);            
});

//WeekDays APIs
Route::group([
    'prefix' => 'week-days'
], function ($router) {
    Route::get('/all', [WeekDaysController::class, 'index']);
    Route::get('/by-id', [WeekDaysController::class, 'getById']);   
});

//service APIs
Route::group([
    'middleware' => ['auth:api','role:Center Admin'],
    'prefix' => 'service'
], function ($router) {
    Route::post('/add', [ServiceController::class, 'store']);
    Route::post('/update', [ServiceController::class, 'update']); 
    Route::post('/delete/{id}', [ServiceController::class, 'deleteById']);      
    Route::post('/delete', [ServiceController::class, 'destroy']);  
}
);         

//service APIs
Route::group([
    'prefix' => 'service'
], function ($router) {
    Route::get('/all', [ServiceController::class, 'index']);
    Route::get('/by-id', [ServiceController::class, 'getById']);
    Route::get('/by-center', [ServiceController::class, 'getByCenterId']);
    Route::get('/by-proCenter-id', [ServiceController::class, 'getByProCenterId']);
}
);



//Category APIs
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'category'
], function ($router) {
    Route::post('/add', [CategoryController::class, 'store']);
    Route::post('/update', [CategoryController::class, 'update']); 
    Route::post('/delete/{id}', [CategoryController::class, 'deleteById']);               
});


//Category APIs
Route::group([
    'prefix' => 'category'
], function ($router) {
    Route::get('/all', [CategoryController::class, 'index']);
    Route::get('/by-id', [CategoryController::class, 'getById']);        
});

//Center APIs
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'center'
], function ($router) {
    Route::post('/add', [CenterController::class, 'store']);
    Route::post('/update', [CenterController::class, 'update']); 
    Route::post('/delete', [CenterController::class, 'deleteById']);          
});


//Center APIs
Route::group([
    'prefix' => 'center'
], function ($router) {
    Route::get('/all', [CenterController::class, 'index']);
    Route::get('/by-id', [CenterController::class, 'getById']);
    Route::get('/by-name', [CenterController::class, 'searchByName']);
    Route::get('/by-cat', [CenterController::class, 'getByCategoryId']);
    Route::get('/search', [CenterController::class, 'dynamicSearch']);               
});

//Comment APIs
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'comment'
], function ($router) {
    Route::post('/add', [CommentController::class, 'store']);
    Route::post('/update', [CommentController::class, 'update']); 
    Route::post('/delete/{id}', [CommentController::class, 'deleteById']);                 
});

//Comment APIs
Route::group([
    'prefix' => 'comment'
], function ($router) {
    Route::get('/all', [CommentController::class, 'index']);
    Route::get('/by-id', [CommentController::class, 'getById']);     
});

//Reservation APIs
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'reservation'
], function ($router) {
    Route::post('/add', [ReservationController::class, 'store']);
    Route::post('/update', [ReservationController::class, 'update']); 
    Route::post('/delete/{id}', [ReservationController::class, 'deleteById']);                 
});

//Reservation APIs
Route::group([
    'prefix' => 'reservation'
], function ($router) {
    Route::get('/all', [ReservationController::class, 'index']);
    Route::get('/by-id', [ReservationController::class, 'getById']);
    Route::get('/by-centerId', [ReservationController::class, 'getreservationByCenterId']);                 
});

//Menu APIs
Route::group([
    'middleware' => ['auth:api','role:Center Admin'],
    'prefix' => 'menu'
], function ($router) {
    Route::post('/add', [MenuController::class, 'store']);
    Route::post('/uploadFile', [MenuController::class, 'uploadFile']);
    Route::post('/update', [MenuController::class, 'update']); 
    Route::post('/delete/{id}', [MenuController::class, 'deleteById']);                 
});

//Menu APIs
Route::group([
    'prefix' => 'menu'
], function ($router) {
    Route::get('/all', [MenuController::class, 'index']);
    Route::get('/by-id', [MenuController::class, 'getById']);
    Route::get('/by-center', [MenuController::class, 'getByCenterId']);                
});

//Post APIs
Route::group([
    'middleware' => ['auth:api',
    'role:Center Admin'],
    'prefix' => 'post'
], function ($router) {
    Route::post('/add', [PostController::class, 'store']);
    Route::post('/update', [PostController::class, 'update']);
    Route::post('/delete/{id}', [PostController::class, 'deleteById']);                 
});

Route::group([
    'prefix' => 'post'
], function ($router) {
    Route::get('/all', [PostController::class, 'index']);
    Route::get('/by-id', [PostController::class, 'getById']);
    Route::get('/center-cat', [PostController::class, 'getByCenterCategory']);
    Route::get('/by-center', [PostController::class, 'searchByCenterName']); 
    Route::get('/by-tag', [PostController::class, 'searchBytag']);               
});

//Property Center APIs
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'property-center'
], function ($router) {
    Route::post('/add', [PropertyCenterController::class, 'store']);
    Route::post('/update', [PropertyCenterController::class, 'update']); 
    Route::post('/delete/{id}', [PropertyCenterController::class, 'deleteById']);                 
});

//Property Center APIs
Route::group([
    'prefix' => 'property-center'
], function ($router) {
    Route::get('/all', [PropertyCenterController::class, 'index']);
    Route::get('/by-center', [PropertyCenterController::class, 'getByCenter']);
    Route::get('/by-id', [PropertyCenterController::class, 'getById']);                
});

//Property APIs
Route::group([
    // 'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'property'
], function ($router) {
    Route::post('/add', [PropertyController::class, 'store']);
    Route::post('/update', [PropertyController::class, 'update']); 
    Route::post('/delete/{id}', [PropertyController::class, 'deleteById']);
    Route::get('/by-cat', [PropertyController::class, 'getByCategory']);                 
});

//Property APIs
Route::group([
    'prefix' => 'property'
], function ($router) {
    Route::get('/all', [PropertyController::class, 'index']);
    Route::get('/by-id', [PropertyController::class, 'getById']);               
});

//Rate Property APIs
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'rate-property'
], function ($router) {
    Route::post('/add', [RatePropertyController::class, 'store']);
    Route::post('/update', [RatePropertyController::class, 'update']); 
    Route::post('/delete/{id}', [RatePropertyController::class, 'deleteById']);                
});

//Rate Property APIs
Route::group([
    'prefix' => 'rate-property'
], function ($router) {
    Route::get('/all', [RatePropertyController::class, 'index']);
    Route::get('/by-id', [RatePropertyController::class, 'getById']);                
});

//Rate APIs
Route::group([
    // 'middleware' => ['auth:api', 'role:User'],
    'prefix' => 'rate'
], function ($router) {
    Route::post('/add', [RateController::class, 'store']);
    Route::get('/all', [RateController::class, 'index']);
    Route::get('/by-id', [RateController::class, 'getById']);
    Route::get('/rate-of-property', [RateController::class, 'getRateOfProerety']); 
    Route::post('/update', [RateController::class, 'update']); 
    Route::post('/delete/{id}', [RateController::class, 'deleteById']);     
    Route::get('/by-center', [RateController::class, 'getByCenterId']);             
});


//Role APIs
Route::group([
    // 'middleware' => ['auth:api','role:Admin'],
    // 'middleware' => 'auth:api',
    'prefix' => 'role'
], function ($router) {
    Route::post('/add-role', [RoleController::class, 'store']);
    Route::get('/all', [RoleController::class, 'index']);            
});

//Permission APIs
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'permission'
], function ($router) {
    Route::post('/add-permission', [PermissionController::class, 'store']);
    Route::get('/all', [PermissionController::class, 'index']);            
});


//RoleHasPermission APIs
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'role-permission'
], function ($router) {
    Route::post('/add', [RoleHasPermissionController::class, 'store']);      
});


//User APIS
Route::group([
    // 'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'user'
], function ($router) {
    Route::post('/add', [UserController::class, 'store']); 
});


//Employee APIS
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'employee'
], function ($router) {
    Route::post('/all', [EmployeeController::class, 'index']); 
});


//Employee APIS
Route::group([
    'middleware' => ['auth:api','role:Center Admin'],
    'prefix' => 'employee'
], function ($router) {
    Route::post('/add', [EmployeeController::class, 'store']); 
    Route::post('/update', [EmployeeController::class, 'update']);
    Route::post('/delete', [EmployeeController::class, 'deleteById']);
});


//EmpService APIS
Route::group([
    'middleware' => ['auth:api','role:Center Admin'],
    'prefix' => 'empservice'
], function ($router) {
    Route::post('/add', [EmpServiceController::class, 'store']); 
    Route::post('/delete', [EmpServiceController::class, 'deleteById']);
});

//EmpService APIS
Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'empservice'
], function ($router) {
    Route::post('/all', [EmpServiceController::class, 'index']);
});

//Attachment APIS
Route::group([
    'middleware' => ['auth:api','role:Admin'],
    'prefix' => 'attachment'
],function ($router){
    Route::post('/add', [AttachmentController::class, 'store']);
});

Route::group([
    'middleware' => 'auth:api'
], function ($router) {
    Route::post('/change-password', [AuthController::class,'changePassword']);
});

Route::get('/language/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'ar'])) {
        abort(400);
    }
    App::setLocale($locale);
});